## Clickadilla
Тестовое задание

## Как развернуть
* Склонировать проект
```
cd ~/
mkdir testovoe
cd testovoe
git clone https://topmostest@bitbucket.org/codexamples/clickadilla.git www
```

> Вместо `~/` используйте более подходящую директорию, если есть необходимость

* Запустить докер
```
ln -s ~/testovoe/www/docker-compose.yml ~/testovoe/docker-compose.yml
ln -s ~/testovoe/www/docker-vhost.conf ~/testovoe/docker-vhost.conf
docker-compose up -d
```

* Зайти в контейнер
```
docker exec -ti clickadilla_php /bin/bash
```

* Действия внутри контейнера
```
composer install
cp .env.example .env
php artisan key:generate --force
php artisan migrate --force
php artisan db:seed --force
```

> Проект доступен по адресу `http://localhost:81`
