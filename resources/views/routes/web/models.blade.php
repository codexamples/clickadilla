@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ studly_case($model) }} ({{ $count }})</div>

                    <div class="panel-body">
                        @if($params)
                            <div class="alert alert-info">
                                <ul>
                                    @foreach($params as $key => $param)
                                        <li>
                                            {{ $key }}
                                            @if(is_array($param))
                                                {{ $param['con'] }}
                                                {{ $param['val'] }}
                                            @else
                                                =
                                                {{ $param }}
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @include('routes.web.models.' . $model)
                        {!! $links !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
