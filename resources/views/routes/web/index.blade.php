@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <a href="{{ route('models', 'film') }}">{{ $films }} film(s)</a>,
                        <a href="{{ route('models', 'hall') }}">{{ $halls }} hall(s)</a>,
                        <a href="{{ route('models', 'show') }}">{{ $shows }} show(s)</a>
                        <hr>
                        @guest
                            <i class="text-muted">You can not re-seed database</i>
                        @else
                            <form
                                    method="post"
                                    action="{{ route('re_seed') }}"
                            >
                                {{ csrf_field() }}
                                <button
                                        class="btn btn-link"
                                        style="padding: 0; margin: 0;"
                                        onclick="return confirm('Are you sure?');"
                                >
                                    Re-seed database
                                </button>
                            </form>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Options</div>

                    <div class="panel-body">
                        <a href="{{ route('cinema') }}">Find shows / Buy tickets</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
