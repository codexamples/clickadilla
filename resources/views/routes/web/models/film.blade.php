<table class="table table-responsive">
    <thead>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>duration</th>
        <th>shows</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        <?php /** @var  \App\Models\Film  $model */ ?>
        <tr>
            <th>{{ $model->id }}</th>
            <td>{{ $model->name }}</td>
            <td>
                @php
                $duration = new \Carbon\Carbon('2000-01-01 00:00:00');
                $duration->addMinutes($model->duration);
                @endphp
                {{ $duration->format('H:i') }}
            </td>
            <td>
                <a href="{{ route('models', ['show', 'film_id' => $model->id]) }}">
                    {{ $model->shows()->count() }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
