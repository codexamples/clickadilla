<table class="table table-responsive">
    <thead>
    <tr>
        <th>id</th>
        <th>places_free</th>
        <th>price_multiplier</th>
        <th>shows</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        <?php /** @var  \App\Models\Hall  $model */ ?>
        <tr>
            <th>{{ $model->id }}</th>
            <td>{{ $model->places_free }}</td>
            <td>{{ number_format($model->price_multiplier, 2) }}</td>
            <td>
                <a href="{{ route('models', ['show', 'hall_id' => $model->id]) }}">
                    {{ $model->shows()->count() }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
