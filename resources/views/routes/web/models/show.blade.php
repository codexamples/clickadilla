<table class="table table-responsive">
    <thead>
    <tr>
        <th>id</th>
        <th>places_bought</th>
        <th>price</th>
        <th>starts_at</th>
        <th>finishes_at</th>
        <th>film</th>
        <th>hall</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        <?php /** @var  \App\Models\Show  $model */ ?>
        <tr>
            <th>{{ $model->id }}</th>
            <td>{{ $model->places_bought }}</td>
            <td>{{ number_format($model->price, 2) }}</td>
            <td>{{ $model->starts_at }}</td>
            <td>{{ $model->finishes_at }}</td>
            <td>
                <a href="{{ route('models', ['film', 'id' => $model->film_id]) }}">
                    # {{ $model->film_id }}
                </a>
            </td>
            <td>
                <a href="{{ route('models', ['hall', 'id' => $model->hall_id]) }}">
                    # {{ $model->hall_id }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
