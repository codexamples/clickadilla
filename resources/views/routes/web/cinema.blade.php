@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cinema ({{ $count }})</div>

                    <div class="panel-body">
                        <form
                                method="get"
                                action="{{ route('cinema') }}"
                        >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="film">Film</label>
                                    <select
                                            name="film_id"
                                            id="film"
                                            class="form-control"
                                    >
                                        <option value="">&lt; Film &gt;</option>
                                        @foreach($films as $film)
                                            <?php /** @var  \App\Models\Film  $film */ ?>
                                            <option
                                                    value="{{ $film->id }}"
                                                    @if(intval(@$filter['film_id']) === $film->id) selected @endif
                                            >
                                                {{ $film->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="from">From</label>
                                    <input
                                            class="form-control"
                                            id="from"
                                            type="text"
                                            name="from"
                                            value="{{ date('H:i', $filter['from']) }}"
                                    >
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="to">To</label>
                                    <input
                                            class="form-control"
                                            id="to"
                                            type="text"
                                            name="to"
                                            value="{{ array_key_exists('to', $filter) ? date('H:i', $filter['to']) : '' }}"
                                            placeholder="HH:MM"
                                    >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-block btn-primary">Search</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                        <div class="col-md-2 col-md-offset-10">
                            <a
                                    href="{{ route('cinema') }}"
                                    class="btn btn-block btn-default"
                            >
                                Reset
                            </a>
                        </div>
                        <div class="clearfix"></div>

                        <hr>

                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>film</th>
                                <th>duration</th>
                                <th>starts</th>
                                <th>finishes</th>
                                <th>hall</th>
                                <th>price</th>
                                <th>places</th>
                                <th>actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($models as $model)
                                <tr>
                                    <th>{{ $model->id }}</th>
                                    <td>{{ $model->film }}</td>
                                    <td>
                                        @php
                                        $duration = new \Carbon\Carbon('2000-01-01 00:00:00');
                                        $duration->addMinutes($model->duration);
                                        @endphp
                                        {{ $duration->format('H:i') }}
                                    </td>
                                    <td>{{ date('H:i', strtotime($model->starts)) }}</td>
                                    <td>{{ date('H:i', strtotime($model->finishes)) }}</td>
                                    <td>{{ $model->hall }}</td>
                                    <td>{{ number_format($model->price, 2) }}</td>
                                    <td>
                                        <span id="places-{{ $model->id }}">{{ $model->places }}</span>
                                        <br>–<br>
                                        {{ $model->places_total }}
                                    </td>
                                    <td>
                                        <button
                                                class="btn btn-info buy-btn"
                                                onclick="window.buyShow('{{ route('api.buy', $model->id) }}', 'places-{{ $model->id }}');"
                                        >
                                            Buy
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr><td colspan="8" class="text-muted">&lt; Пусто &gt;</td></tr>
                            @endforelse
                            </tbody>
                        </table>

                        {!! $links !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.toggleButtons = function(off) {
          var elements = document.getElementsByClassName('buy-btn');

          for (var i = 0; i < elements.length; i++) {
            elements[i].disabled = off;
          }
        };

        window.buyShow = function(url, placesId) {
          window.toggleButtons(true);

          var xhr = new XMLHttpRequest();

          xhr.open('POST', url, true);
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

          xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
              if (xhr.status === 200) {
                var span = document.getElementById(placesId);

                span.innerHTML = (parseInt(span.innerHTML) - 1).toString();

                alert('Succeeded!');
              } else {
                alert(xhr.responseText);
              }

              window.toggleButtons(false);
            }
          };

          xhr.send();
        };
    </script>
@endsection
