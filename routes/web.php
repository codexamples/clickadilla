<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
*/

Route::group([
    'middleware' => 'guest',
    'prefix' => 'login'
], function () {
    Route::get('/', 'LoginController@showLoginForm')->name('login');
    Route::post('/', 'LoginController@login');
});

Route::post('/logout', 'LoginController@logout')
    ->name('logout')
    ->middleware('auth');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'IndexController@index');
Route::get('/cinema', 'CinemaController@index')->name('cinema');

Route::post('/re-seed', 'ReSeedController@index')
    ->name('re_seed')
    ->middleware('auth');

Route::get('/{model}', 'ModelsController@index')->name('models');
