<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
*/

Route::post('/buy/{show_id}', 'BuyController@index')->name('api.buy');
