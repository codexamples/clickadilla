<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Hall Factory
|--------------------------------------------------------------------------
|
*/

$factory->define(App\Models\Hall::class, function (Faker $faker) {
    return [
        'places_free' => rand(30, 120),
        'price_multiplier' => rand(1, 10) / rand(1, 10),
    ];
});
