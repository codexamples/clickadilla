<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Film Factory
|--------------------------------------------------------------------------
|
*/

$factory->define(App\Models\Film::class, function (Faker $faker) {
    return [
        'name' => array_random([
            $faker->colorName . ' ' . $faker->name,
            $faker->address,
            $faker->lastName . ' in ' . $faker->monthName,
        ]),
        'duration' => array_random([60, 90, 120, 155]),
    ];
});
