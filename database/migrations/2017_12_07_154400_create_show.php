<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_id');
            $table->unsignedInteger('hall_id');
            $table->unsignedMediumInteger('places_bought');
            $table->double('price');
            $table->time('starts_at');
            $table->time('finishes_at');
            $table->timestamps();

            $table
                ->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table
                ->foreign('hall_id')
                ->references('id')
                ->on('halls')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
