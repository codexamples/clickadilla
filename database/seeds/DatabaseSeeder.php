<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->call(UserSeeder::class);
        } catch (\Exception $ex) {
            return;
        }

        $this->call([
            FilmSeeder::class,
            HallSeeder::class,
            ShowSeeder::class,
        ]);
    }
}
