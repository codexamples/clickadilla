<?php

use Illuminate\Database\Seeder;

class ShowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $films = \App\Models\Film::all();
        $halls = \App\Models\Hall::all();
        $prices = config('cinemas.prices');

        /**
         * Возвращает цену, которая зависит от времени
         *
         * @param  \Carbon\Carbon  $time
         * @return double
         */
        $search_price = function (\Carbon\Carbon $time) use ($prices) {
            foreach ($prices as $price) {
                if (is_array($price)) {
                    if ($time->hour >= $price['from'] && $time->hour <= $price['to']) {
                        return $price['price'];
                    }
                }
            }

            return 0.0;
        };

        // let's do it ... later ;)

        foreach ($halls as $hall) {
            /** @var  \App\Models\Hall  $hall */
            /** @var  \App\Models\Film  $film */

            $start = new \Carbon\Carbon('2000-01-01 00:00:00');

            while (true) {
                $film = $films->random();

                $finish = new \Carbon\Carbon($start->toDateTimeString());
                $finish->addMinutes($film->duration);

                if ($start->day > 1 || $finish->day > 1) {
                    break;
                }

                \App\Models\Show::create([
                    'film_id' => $film->id,
                    'hall_id' => $hall->id,
                    'places_bought' => rand(0, $hall->places_free),
                    'price' => call_user_func($search_price, $start) * $hall->price_multiplier,
                    'starts_at' => $start,
                    'finishes_at' => $finish,
                ]);

                $start = $finish;
            }
        }
    }
}
