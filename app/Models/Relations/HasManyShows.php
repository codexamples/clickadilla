<?php

namespace App\Models\Relations;

use App\Models\Show;
use ReflectionClass;

/**
 * Связь с моделью показа один-ко-многим, как родитель
 *
 * @property  \App\Models\Show[]|\Illuminate\Database\Eloquent\Collection  $shows  "Список дочерних показов"
 */
trait HasManyShows
{
    /**
     * Show - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shows()
    {
        return $this->hasMany(Show::class, $this->getShowForeignKey());
    }

    /**
     * Возвращает внешний ключ для связи с показом
     *
     * @return string
     */
    protected function getShowForeignKey()
    {
        $reflection = new ReflectionClass($this);

        return implode('_', [
            snake_case($reflection->getShortName()),
            $this->primaryKey,
        ]);
    }
}
