<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Показ (сеанс) фильма
 *
 * @property  int  $id
 * @property  int  $film_id  "Идентификатор фильма"
 * @property  int  $hall_id  "Идентификатор зала"
 * @property  int  $places_bought  "Количество уже приобретенных посадочных мест"
 * @property  double  $price  "Итоговая цена за билет"
 * @property  \Carbon\Carbon  $starts_at  "Время, когда стартует сеанс"
 * @property  \Carbon\Carbon  $finishes_at  "Время, когда сеанс заканчивается"
 * @property  \App\Models\Film  $film  "Фильм, который будет показан"
 * @property  \App\Models\Hall  $hall  "Зал, в котором будет показан фильм"
 */
class Show extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'film_id',
        'hall_id',
        'places_bought',
        'price',
        'starts_at',
        'finishes_at',
    ];

    /**
     * Film - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function film()
    {
        return $this->belongsTo(Film::class, 'film_id');
    }

    /**
     * Hall - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hall()
    {
        return $this->belongsTo(Hall::class, 'hall_id');
    }
}
