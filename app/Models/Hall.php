<?php

namespace App\Models;

use App\Models\Relations\HasManyShows;
use Illuminate\Database\Eloquent\Model;

/**
 * Зал
 *
 * @property  int  $id
 * @property  int  $places_free  "Количество посадочных мест"
 * @property  double  $price_multiplier  "Коэффициент цены за билет"
 */
class Hall extends Model
{
    use HasManyShows;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'places_free',
        'price_multiplier',
    ];
}
