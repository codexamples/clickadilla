<?php

namespace App\Models;

use App\Models\Relations\HasManyShows;
use Illuminate\Database\Eloquent\Model;

/**
 * Фильм
 *
 * @property  int  $id
 * @property  string  $name  "Название фильма"
 * @property  int  $duration  "Продолжительность в минутах"
 */
class Film extends Model
{
    use HasManyShows;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'duration',
    ];
}
