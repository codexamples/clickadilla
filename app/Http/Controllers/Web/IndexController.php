<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Film;
use App\Models\Hall;
use App\Models\Show;

class IndexController extends Controller
{
    /**
     * Главная страница приложения
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('routes.web.index', [
            'films' => Film::count(),
            'halls' => Hall::count(),
            'shows' => Show::count(),
        ]);
    }
}
