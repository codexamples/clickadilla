<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use DatabaseSeeder;
use Illuminate\Support\Facades\DB;

class ReSeedController extends Controller
{
    /**
     * Пересоздание БД
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        DB::statement('SET foreign_key_checks=0');

        foreach (['Show', 'Hall', 'Film', 'User'] as $model) {
            $class = '\App\Models\\' . $model;
            $class::query()->truncate();
        }

        DB::statement('SET foreign_key_checks=1');

        /** @var  \DatabaseSeeder  $seeder */
        $seeder = app()->make(DatabaseSeeder::class);

        $seeder->run();
        return redirect()->back(); // ->with('status', 'Re-seeding has been succeeded');
    }
}
