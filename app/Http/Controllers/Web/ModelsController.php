<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ModelsController extends Controller
{
    /**
     * Вывод всех моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $model
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function index(Request $request, string $model)
    {
        try {
            $class = '\App\Models\\' . studly_case($model);
            $params = $request->except(['page']);

            /** @var  \Illuminate\Database\Eloquent\Builder  $q */
            $q = $class::query();

            foreach ($params as $key => $value) {
                try {
                    if (is_array($value)) {
                        $q->where($key, $value['con'], $value['val']);
                    } else {
                        $q->where($key, $value);
                    }
                } catch (Exception $ex) {
                }
            }

            try {
                $models = $q->paginate(10);
            } catch (Exception $ex) {
                $models = [];
            }

            return view('routes.web.models', [
                'model' => $model,
                'count' => $models ? $models->total() : 0,
                'params' => $params,
                'models' => $models,
                'links' => $models ? $models->appends($params)->links() : '',
            ]);
        } catch (Exception $ex) {
            throw new NotFoundHttpException();
        }
    }
}
