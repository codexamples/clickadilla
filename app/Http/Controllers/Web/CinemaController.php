<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Film;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class CinemaController extends Controller
{
    /**
     * Поиск сеансов кино
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [
            'films' => Film::all(),
            'filter' => [],
        ];

        // filter

        if ($request->input('film_id') !== null) {
            $data['filter']['film_id'] = $request->input('film_id');
        }

        $data['filter']['from'] = $request->input('from') === null
            ? time()
            : strtotime($request->input('from'));

        if ($request->input('to') !== null) {
            $data['filter']['to'] = strtotime($request->input('to'));
        }

        $filter = $data['filter'];
        $filter['from'] = date('H:i', $filter['from']);

        if (array_key_exists('to', $filter)) {
            $filter['to'] = date('H:i', $filter['to']);
        }

        // query

        /** @var  \Illuminate\Database\Query\Builder  $q */
        $q = DB::table('shows');

        $q
            ->selectRaw("
                shows.id as id,
                films.name as film,
                films.duration as duration,
                shows.starts_at as starts,
                shows.finishes_at as finishes,
                concat('# ', halls.id) as hall,
                shows.price as price,
                (halls.places_free - shows.places_bought) as places,
                halls.places_free as places_total,
                shows.film_id as film_id
            ")
            ->leftJoin('films', 'films.id', '=', 'shows.film_id')
            ->leftJoin('halls', 'halls.id', '=', 'shows.hall_id')
            ->having('places', '>', 0);

        if (array_key_exists('film_id', $data['filter'])) {
            $q->having('film_id', '=', $data['filter']['film_id']);
        }

        $q->having('starts', '>=', date('H:i:s', $data['filter']['from']));

        if (array_key_exists('to', $data['filter'])) {
            $q->having('starts', '<=', date('H:i:s', $data['filter']['to']));
        }

        $per_page = 10;
        $cur_page = Paginator::resolveCurrentPage();
        $count_q = clone $q;

        $data['models'] = $count_q->forPage($cur_page, $per_page)->get();

        $data['count'] = $q
            ->get()
            ->count();

        $data['links'] = (new LengthAwarePaginator($data['models'], $data['count'], $per_page))
            ->setPath(route('cinema'))
            ->appends($filter)
            ->links();

        // result

        return view('routes.web.cinema', $data);
    }
}
