<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Show;
use Exception;

class BuyController extends Controller
{
    /**
     * Покупка билета на сеанс
     *
     * @param  int  $show_id
     * @return \Illuminate\Http\Response
     */
    public function index(int $show_id)
    {
        try {
            /** @var  \App\Models\Show  $show */
            $show = Show::find($show_id);

            if ($show === null) {
                throw new Exception('Show not found');
            }

            if (time() - strtotime($show->starts_at) > 1200) {
                throw new Exception('It\'s too late');
            }

            if ($show->places_bought >= $show->hall->places_free) {
                throw new Exception('Not enough places to buy');
            }

            $show->places_bought++;
            $show->save();

            return response()->make();
        } catch (Exception $ex) {
            return response()->make($ex->getMessage(), 400);
        }
    }
}
